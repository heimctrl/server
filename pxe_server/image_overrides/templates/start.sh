#!/bin/bash

BUSDRIVER_IMAGE="backplane:80/heimctrl/bus-driver:latest"
CONTAINER_NAME="bus-driver-container"

function run_container() {
  if docker ps -a --format '{{.Names}}' | grep -Eq "^${CONTAINER_NAME}\$"; then
    docker rm -f $CONTAINER_NAME
  fi
   
  HOSTNAME=$(hostname)
  HA_IP=$(ping -c 1 homeassistant.local | grep PING | awk -F'[()]' '{print $2}')

  DOCKER_OPTS="--insecure-registry=backplane:80" docker run \
    -d --name $CONTAINER_NAME \
    --net=host \
    --health-cmd='node /opt/bus-driver/healthcheck.js || exit 2' \
    --health-start-period=2s \
    --health-interval=60s \
    --health-timeout=2s \
    --restart=unless-stopped \
    --env NODE_ENV=production \
    --env PORT=8123 \
    --env DATABASE_LOCATION=/opt/bus-driver/database \
    --env SERVER_ROOM=$HOSTNAME \
    --env SERVER_HOMEASSISTANT_URL=http://$HA_IP:8123 \
    --env SERVER_HOMEASSISTANT_MQTT_BROKER=mqtt://$HA_IP:1883 \
    --env SERVER_HOMEASSISTANT_MQTT_USERNAME=panel \
    --env SERVER_HOMEASSISTANT_MQTT_PASSWORD=panel \
    --env SERVER_HOMEASSISTANT_DEFAULTDASHBOARD=/dashboard-panels/{ROOM} \
    --label location=dev \
    --log-driver=journald \
    --log-opt env=NODE_ENV \
    --log-opt labels=location \
    --expose 8123 \
    -p 8123:8123 \
    -v ~/bus-driver:/opt/bus-driver/database \
    $BUSDRIVER_IMAGE
}

function update_and_restart_container() {
  # Pull the latest version of the image
  docker pull $BUSDRIVER_IMAGE

  # Check if the Docker image is running
  if docker ps | grep -q "$CONTAINER_NAME"; then
    echo "Container $CONTAINER_NAME is already running. Checking for image update..."

    # Check if the pulled image ID is different from the running container's image ID
    running_image_id=$(docker inspect --format='{{.Image}}' "$CONTAINER_NAME")
    latest_image_id=$(docker inspect --format='{{.Id}}' "$BUSDRIVER_IMAGE")

    if [ "$running_image_id" != "$latest_image_id" ]; then
      echo "New image available. Restarting the container $CONTAINER_NAME..."
      docker stop "$CONTAINER_NAME" && docker rm "$CONTAINER_NAME"
      run_container
    else
      echo "No new image available. No action required."
    fi
  else
    echo "Container $CONTAINER_NAME is not running. Starting a new container..."
    run_container
  fi

  # Wait for the container to be ready
  echo "Waiting for the container to be ready..."
  until [ "$(docker inspect -f {{.State.Health.Status}} $CONTAINER_NAME)" == "healthy" ]; do
    container_status=$(docker inspect -f {{.State.Status}} $CONTAINER_NAME)
    container_exit_code=$(docker inspect -f {{.State.ExitCode}} $CONTAINER_NAME)
    
    if ([ "$container_status" == "exited" ] || [ "$container_status" == "restarting" ]) && [ "$container_exit_code" != "0" ] && [ "$container_exit_code" != "2" ]; then
      echo "Container has exited unexpectedly with exit code $container_exit_code. Printing logs:"
      docker logs $CONTAINER_NAME
      docker stop $CONTAINER_NAME
      read -p "Press any key to continue"
      exit 1
    fi

    sleep 1;
  done
  echo "Container is now ready."

  # remove dangling images
  docker image prune -f
}

function start_browser() {
  echo "Opening browser..."

  # rotate logs
  if [ -f chromium.log ]; then
    mv chromium.log chromium.log.1
    mv chromium-err.log chromium-err.log.1
  fi
  if [ -f firefox.log ]; then
    mv firefox.log firefox.log.1
    mv firefox-err.log firefox-err.log.1
  fi

  # If Chromium crashes (usually due to rebooting), clear the crash flag so we don't have the annoying warning bar
  # sed -i 's/"exited_cleanly":false/"exited_cleanly":true/' /home/panel/.config/chromium/Default/Preferences
  # sed -i 's/"exit_type":"Crashed"/"exit_type":"Normal"/' /home/panel/.config/chromium/Default/Preferences

  # start chromium browser in kiosk mode
  # nohup chromium-browser --kiosk --app=http://localhost:8123/ui/panel > chromium.log 2> chromium-err.log &
  # nohup chromium-browser --disable-gpu --start-maximized --app=http://localhost:8123/ui/panel > chromium.log 2> chromium-err.log &
  nohup firefox -foreground -private-window http://localhost:8123/ui/panel > firefox.log 2> firefox-err.log &
  disown
}

update_and_restart_container
start_browser

sleep 10
# read -p "Press any key to continue"
