#!/bin/bash

DOMAIN=$(cat /etc/hostname)
echo "Using domain: $DOMAIN"

VERSION=v2.10.0
FILE="harbor-online-installer-$VERSION.tgz"

if [ ! -f "$FILE" ]; then
  wget "https://github.com/goharbor/harbor/releases/download/$VERSION/$FILE.asc"
  wget "https://github.com/goharbor/harbor/releases/download/$VERSION/$FILE"
fi

gpg --keyserver hkps://keyserver.ubuntu.com --receive-keys 644FF454C0B4115C
gpg -v --keyserver hkps://keyserver.ubuntu.com --verify harbor-online-installer-$VERSION.tgz.asc

tar xzvf harbor-online-installer-$VERSION.tgz

if [ -f "harbor/harbor.yml" ]; then
  # make backup
  cp harbor/harbor.yml harbor/harbor.yml.bak
fi

cp harbor/harbor.yml.tmpl harbor/harbor.yml
sed -i "s/reg.mydomain.com/$DOMAIN/g" harbor/harbor.yml
sed -i '/# https related config/,/private_key: \/your\/private\/key\/path/s/^/#/' harbor/harbor.yml

harbor/install.sh
