HeimCtrl server installation scripts
====================================

# PXE Server

Network booot server and drive for the room panels.

# Harbor

Docker registry server for the bus-driver software deployment.

# Single server setup

Instead of multiple servers it should be sufficient for most cases to have all of the backend services installed on a single server.

1. Installs Docker on the server
2. Runs network boot (PXE) server within docker
3. Runs Harbor within Docker
