#!/bin/bash

# Check for root privileges
if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root"
   exit 1
fi

# Parameter validation
if [ -z "$1" ]; then
  echo "Usage: $0 SERIAL [ARGS]"
  exit 1
fi

SERIAL=$1
shift
ROOT_FOLDER=/nfs/$SERIAL

read_journal() {
  # Copy qemu-arm-static to the chroot environment to enable ARM emulation
  cp /usr/bin/qemu-aarch64-static $ROOT_FOLDER/usr/bin/

  # Ensure we are in the chroot environment using qemu-arm-static for ARM emulation
  chroot $ROOT_FOLDER /usr/bin/qemu-aarch64-static /bin/bash <<EOF
    mount -t proc proc /proc
    journalctl -n 100 $*
    umount -t proc proc /proc
EOF

  # Clean up by removing qemu-arm-static from the chroot environment
  rm $ROOT_FOLDER/usr/bin/qemu-aarch64-static
}

read_journal "$@"

echo "All done."
