#!/bin/bash

SERIAL=$1
ROOT_FOLDER=/nfs/$SERIAL

# Check for root privileges
if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root"
   exit 1
fi

if [ ! -d "${ROOT_FOLDER}/" ]; then 
  echo "Image does not exist: ${ROOT_FOLDER}/"
  exit 0
fi


echo "Delete image folder"
rm -rf $ROOT_FOLDER/

echo "Delete tftp folder"
umount "/tftpboot/$SERIAL"
rm -rf "/tftpboot/$SERIAL"
sed "/$SERIAL/d" -i /etc/fstab
sed "/$SERIAL/d" -i /etc/exports

systemctl restart rpcbind
systemctl restart nfs-kernel-server

echo "Delete iscsi data and configuration"
ISCSI_NAME="pi$SERIAL"
rm -f "/var/lib/iscsi_disks/${ISCSI_NAME}.img"
rm -f "/etc/tgt/conf.d/${ISCSI_NAME}.conf"
systemctl restart tgt

echo "All done."
