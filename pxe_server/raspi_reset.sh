#!/bin/bash

# Check for root privileges
if [[ $EUID -ne 0 ]]; then
  echo "This script must be run as root"
  exit 1
fi

# Parameter validation
if [ -z "$1" ] || [ -z "$2" ]; then
  echo "Usage: $0 SERIAL HOSTNAME [PASSWORD]"
  exit 1
fi

SERIAL=$1
HOSTNAME=$2
PASSWORD=${3:-panel} # Default password if not provided

ROOT_FOLDER=/nfs/$SERIAL
BOOT_FOLDER=$ROOT_FOLDER/boot
FIRMWARE_FOLDER=$BOOT_FOLDER/firmware

# Robust IP address retrieval
SERVER_IP_BASE=$(ip -o -4 addr show scope global | grep 'inet 192')
if [ -z "$SERVER_IP_BASE" ]; then
  echo "Failed to find an active network interface with an IP address."
  exit 1
fi
SERVER_IP=$(echo "$SERVER_IP_BASE" | awk '{print $4}' | cut -d/ -f1)
echo "Using server IP: $SERVER_IP"

# Ensure ROOT_FOLDER exists
if [ ! -d "${ROOT_FOLDER}/" ]; then
  echo "Image folder does not exist: ${ROOT_FOLDER}/"
  exit 1
fi

function backup_file() {
  file=$1
  # Check if the source file exists
  if [ -e "$file" ]; then
    # Only proceed with backup if a backup file does not already exist
    if [ ! -e "$file.bak" ]; then
      cp -np "$file" "$file.bak"
    fi
  else
    echo "Source file $file does not exist, skipping backup."
  fi
}

# copy overrides
echo "Copy overrides"
backup_file "$FIRMWARE_FOLDER/config.txt"
rsync -ai --no-perms --no-owner --no-group image_overrides/fs/ "$ROOT_FOLDER/" | egrep '^>'
rsync -ai --no-perms --no-owner --no-group image_overrides/boot/ "$BOOT_FOLDER/" | egrep '^>'

echo "Configure image"
# enable SSH using the RPi wizard
touch "$FIRMWARE_FOLDER/ssh"
# remove local fs mount but put firmware location back so the Pi scrips still work
# should prevent error "firmware location not found"
backup_file "$ROOT_FOLDER/etc/fstab"
sed -i /PARTUUID/d "$ROOT_FOLDER/etc/fstab"
if ! grep -q "^/boot/firmware" "$ROOT_FOLDER/etc/fstab"; then
  echo "/boot/firmware /boot/firmware none defaults,bind 0 0" | tee -a $ROOT_FOLDER/etc/fstab
fi
# create cmdlint.txt pointing to nfs server
backup_file "$FIRMWARE_FOLDER/cmdline.txt"
echo "console=serial0,115200 console=tty1 root=/dev/nfs nfsroot=$SERVER_IP:$ROOT_FOLDER,vers=3 rw ip=dhcp rootfstype=nfs rootwait quiet plymouth.ignore-serial-consoles" | tee "$FIRMWARE_FOLDER/cmdline.txt"
# set file explorer options
backup_file "$ROOT_FOLDER/etc/xdg/libfm/libfm.conf"
sed -i -e 's/single_click=0/single_click=1/' $ROOT_FOLDER/etc/xdg/libfm/libfm.conf

configure_hostname() {
  # use default password if none set
  if [ -z "$HOSTNAME" ]; then
    echo "No HOSTNAME specified, will be raspberrypi"
  else
    echo "Setup hostname"
    echo $HOSTNAME | tee "$ROOT_FOLDER/etc/hostname"
    sed /raspberrypi/d -i "$ROOT_FOLDER/etc/hosts"

    sed /$HOSTNAME/d -i "$ROOT_FOLDER/etc/hosts"
    echo "127.0.0.1		$HOSTNAME" | tee -a "$ROOT_FOLDER/etc/hosts"

    sed /backplane/d -i "$ROOT_FOLDER/etc/hosts"
    echo "$SERVER_IP		backplane" | tee -a "$ROOT_FOLDER/etc/hosts"
  fi
}

create_panel_user() {
  echo "Create user 'panel'"

  # Copy qemu-arm-static to the chroot environment to enable ARM emulation
  cp /usr/bin/qemu-aarch64-static $ROOT_FOLDER/usr/bin/

  # Ensure we are in the chroot environment using qemu-arm-static for ARM emulation
  chroot $ROOT_FOLDER /usr/bin/qemu-aarch64-static /bin/bash <<EOF
    if getent passwd panel > /dev/null; then
      echo "User panel already exists, skipping creation"
    else
      adduser --disabled-password --gecos "" panel
      echo "panel:$PASSWORD" | chpasswd
      # usermod --login panel panel
      usermod -aG sudo panel
    fi

    mount -t proc proc /proc

    getent passwd pi > /dev/null
    if getent passwd pi  > /dev/null; then
      echo "Remove pi user"
      deluser --remove-home pi
    fi

    if getent passwd rpi-first-boot-wizard > /dev/null; then
      echo "Remove rpi-first-boot-wizard user"
      deluser --remove-home rpi-first-boot-wizard
    fi

    umount -t proc proc /proc
EOF

  # Clean up by removing qemu-arm-static from the chroot environment
  rm $ROOT_FOLDER/usr/bin/qemu-aarch64-static
}

configure_auto_login() {
  echo "Configure auto login for user 'panel'"

  if test -f "$ROOT_FOLDER/etc/lightdm/lightdm.conf"; then
    backup_file "$ROOT_FOLDER/etc/lightdm/lightdm.conf"
    sed -i -e "s/^\(#\|\)autologin-user=.*/autologin-user=panel/" "$ROOT_FOLDER/etc/lightdm/lightdm.conf"
    sed -i -e "s/^\(#\|\)exit-on-failure=.*/exit-on-failure=false/" "$ROOT_FOLDER/etc/lightdm/lightdm.conf"

    backup_file "$ROOT_FOLDER/etc/lightdm/lightdm-gtk-greeter.conf"
    sed -i -e "s/^\(#\|\)screensaver-timeout=.*/screensaver-timeout=10/" "$ROOT_FOLDER/etc/lightdm/lightdm-gtk-greeter.conf"

    # disable the first boot wizard
    rm "$ROOT_FOLDER/etc/xdg/autostart/piwiz.desktop"
  else
    echo "No lightdm.conf found at $ROOT_FOLDER/etc/lightdm/lightdm.conf"
    true
  fi

  # configure screen timeout
  backup_file "$ROOT_FOLDER/etc/wayfire/defaults.ini"
  sed -i -e "s/^\(#\|\)screensaver_timeout =.*/screensaver_timeout = 10/" "$ROOT_FOLDER/etc/wayfire/defaults.ini"
  sed -i -e "s/^\(#\|\)dpms_timeout =.*/dpms_timeout = 10/" "$ROOT_FOLDER/etc/wayfire/defaults.ini"
  sed -i /disable_on_fullscreen/d "$ROOT_FOLDER/etc/wayfire/defaults.ini"
  # this makes sure screen timeout is still in effect when in fullscreen
  sed -i '/^\[idle\]$/a disable_on_fullscreen = false' "$ROOT_FOLDER/etc/wayfire/defaults.ini"
}

install_docker() {
  echo "Install additional packages"
  
  # Copy qemu-arm-static to the chroot environment to enable ARM emulation
  cp /usr/bin/qemu-aarch64-static $ROOT_FOLDER/usr/bin/

  # Ensure we are in the chroot environment using qemu-arm-static for ARM emulation
  chroot $ROOT_FOLDER /usr/bin/qemu-aarch64-static /bin/bash <<EOF
    if dpkg -l | grep -qw docker-ce; then
      exit 0
    fi

    # Add Docker's official GPG key:
    apt-get update
    apt-get install -y ca-certificates curl gnupg
    install -m 0755 -d /etc/apt/keyrings
    rm -f /etc/apt/keyrings/docker.gpg
    curl -fsSL https://download.docker.com/linux/debian/gpg | gpg --dearmor -o /etc/apt/keyrings/docker.gpg
    chmod a+r /etc/apt/keyrings/docker.gpg

    # Add the repository to Apt sources:
    echo \
      "deb [arch=arm64 signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian \
      $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | \
      tee /etc/apt/sources.list.d/docker.list > /dev/null

    apt-get update
    apt-get install -y docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin

    # enable access to non-root users
    usermod -aG docker panel

    # add insecure registry
    grep -q '^DOCKER_OPTS' /etc/default/docker || echo 'DOCKER_OPTS="--insecure-registry backplane:80"' | tee -a /etc/default/docker
    echo '{"insecure-registries":["backplane:80"]}' > /etc/docker/daemon.json 
EOF

  # Clean up by removing qemu-arm-static from the chroot environment
  rm $ROOT_FOLDER/usr/bin/qemu-aarch64-static
}

create_iscsi_target() {
  # install open-iscsi initiator as docker backing disk
  echo "Create iscsi target"

  ISCSI_SERVER="iqn.2024-01.backplane.srv"
  ISCSI_NAME="pi$SERIAL"
  ISCSI_ROOT=/var/lib/iscsi_disks
  ISCSI_TARGET=$ISCSI_SERVER:$ISCSI_NAME

  dd if=/dev/zero of=$ISCSI_ROOT/$ISCSI_NAME.img count=0 bs=1 seek=2G
  mkfs.ext4 $ISCSI_ROOT/$ISCSI_NAME.img
  tune2fs -c0 -i0 $ISCSI_ROOT/$ISCSI_NAME.img

  cat <<EOF > "/etc/tgt/conf.d/$ISCSI_NAME.conf"
# create new
# if you set some devices, add <target>-</target> and set the same way with follo>
# naming rule : [ iqn.(year)-(month).(reverse of domain name):(any name you like)>
<target $ISCSI_TARGET>
    # provided device as a iSCSI target
    backing-store $ISCSI_ROOT/$ISCSI_NAME.img
    # iSCSI Initiator's IQN you allow to connect
    # initiator-name iqn.2024-01.panel.srv:$ISCSI_NAME
    # authentication info ( set anyone you like for "username", "password" )
    # incominguser panel panel
</target>
EOF

  systemctl restart tgt

  echo "Install open-iscsi initiator"

  sed /sda/d -i "$ROOT_FOLDER/etc/fstab"
  echo "/dev/sda /var/lib/docker ext4 _netdev 0 0" | tee -a $ROOT_FOLDER/etc/fstab

  ISCSI_NODE="$ROOT_FOLDER/etc/iscsi/nodes/$ISCSI_SERVER:$ISCSI_NAME/$SERVER_IP,3260,1/default"
  mkdir -p "$ROOT_FOLDER/etc/iscsi/nodes/$ISCSI_SERVER:$ISCSI_NAME/$SERVER_IP,3260,1"
  cp image_overrides/templates/iscsi_node $ISCSI_NODE
  sed -i -e "s/NODE_NAME/$ISCSI_TARGET/" $ISCSI_NODE
  sed -i -e "s/SERVER_IP/$SERVER_IP/" $ISCSI_NODE

  # Copy qemu-arm-static to the chroot environment to enable ARM emulation
  cp /usr/bin/qemu-aarch64-static $ROOT_FOLDER/usr/bin/

  # Ensure we are in the chroot environment using qemu-arm-static for ARM emulation
  chroot $ROOT_FOLDER /usr/bin/qemu-aarch64-static /bin/bash <<EOF
    mount -t proc proc /proc

    apt install -y open-iscsi
    systemctl enable open-iscsi

    umount -t proc proc /proc
EOF

  # Clean up by removing qemu-arm-static from the chroot environment
  rm $ROOT_FOLDER/usr/bin/qemu-aarch64-static
}

configure_kiosk() {
  echo "Configure kiosk script"

  cp image_overrides/templates/start.sh $ROOT_FOLDER/home/panel/
  mkdir -p  $ROOT_FOLDER/home/panel/.config/autostart/
  cp image_overrides/templates/panel_kiosk.desktop $ROOT_FOLDER/home/panel/.config/autostart/
  mkdir -p  $ROOT_FOLDER/home/panel/Desktop/
  cp image_overrides/templates/*.desktop $ROOT_FOLDER/home/panel/Desktop/

  # Copy qemu-arm-static to the chroot environment to enable ARM emulation
  cp /usr/bin/qemu-aarch64-static $ROOT_FOLDER/usr/bin/

  # Ensure we are in the chroot environment using qemu-arm-static for ARM emulation
  chroot $ROOT_FOLDER /usr/bin/qemu-aarch64-static /bin/bash <<EOF
    chown panel:panel /home/panel/start.sh
    chmod +x /home/panel/start.sh

    chown panel:panel -R /home/panel/.config
    chown panel:panel -R /home/panel/Desktop

    # set dark theme
    gsettings set org.gnome.desktop.interface gtk-theme "PiXnoir"
EOF

  # Clean up by removing qemu-arm-static from the chroot environment
  rm $ROOT_FOLDER/usr/bin/qemu-aarch64-static
}

configure_hostname
create_panel_user
configure_auto_login
install_docker
create_iscsi_target
configure_kiosk

echo "Reset done."
