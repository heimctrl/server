PXE Server
==========

Setting up a network boot (PXE) server for the panels.

## Tutorials

Tutorials used to create the scripts.

### Enable USB boot (and network boot order)

https://www.raspberrypi.org/documentation/hardware/raspberrypi/bootmodes/msd.md

### Installing the PXE server

https://hackaday.com/2019/11/11/network-booting-the-pi-4/

https://linuxhit.com/raspberry-pi-pxe-boot-netbooting-a-pi-4-without-an-sd-card/

## Monitoring

Watch connections to the server:

    sudo tail -f /var/log/daemon.log

## Get Raspberry Pi serial

cat /proc/cpuinfo

or

vcgencmd otp_dump | grep '28:' 

use MAC address

ethtool --show-permaddr eth0
