#!/bin/bash

ISCSI_ROOT=/var/lib/iscsi_disks

sudo apt-get update
sudo apt-get -y install unzip kpartx dnsmasq nfs-kernel-server rsync

sudo systemctl enable rpcbind
sudo systemctl enable nfs-kernel-server

sudo mkdir -p /tftpboot
sudo chmod 777 /tftpboot

sudo cp -ar server_overrides/ server_tmp/
sudo chown -R root:root server_tmp
sudo chmod -R 755 server_tmp
sudo cp -a server_tmp/* /
sudo rm -r server_tmp/

# enable dnsmasq config override folder
echo 'conf-dir=/etc/dnsmasq.d/,*.conf' | sudo tee -a /etc/dnsmasq.conf

sudo systemctl enable systemd-networkd
sudo systemctl enable dnsmasq
sudo systemctl restart dnsmasq

echo "Install iscsi target"
sudo apt-get -y install tgt
sudo mkdir -p $ISCSI_ROOT
