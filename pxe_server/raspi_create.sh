#!/bin/bash

SERIAL=$1
ROOT_FOLDER=/nfs/$SERIAL
BOOT_FOLDER=/tftpboot/$SERIAL
FIRMWARE_FOLDER=$ROOT_FOLDER/boot/firmware

# Check for root privileges
if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root"
   exit 1
fi

if [ -d "${ROOT_FOLDER}/" ]; then
  echo "Image folder already exists ${ROOT_FOLDER}/"
  exit 0
fi

RASPBIAN_WEB=https://downloads.raspberrypi.org/raspios_arm64/images/raspios_arm64-2023-12-06
RASPBIAN_IMG=2023-12-05-raspios-bookworm-arm64
RASPBIAN_EXT=.img.xz

if [ ! -f "images/${RASPBIAN_IMG}${RASPBIAN_EXT}" ]; then
  echo "Downloading image ${RASPBIAN_IMG}${RASPBIAN_EXT}"
  mkdir -p images
  wget -P images ${RASPBIAN_WEB}/${RASPBIAN_IMG}${RASPBIAN_EXT}
fi
if [ ! -f "images/${RASPBIAN_IMG}.img" ]; then
  echo "Extracting image ${RASPBIAN_IMG}${RASPBIAN_EXT}"
  xz -dkv "images/${RASPBIAN_IMG}${RASPBIAN_EXT}"
fi

echo "Mounting image"
# find the correct loop device where the image will be mounted, this can change when we change the image
LOOP_DEVICE=$(kpartx -lv "images/${RASPBIAN_IMG}.img" | awk '{print substr($1,1,5)} NR==1{exit}')
kpartx -av "images/${RASPBIAN_IMG}.img"
mkdir -p images/rootmnt
mkdir -p images/bootmnt
mount /dev/mapper/${LOOP_DEVICE}p2 images/rootmnt/
mount /dev/mapper/${LOOP_DEVICE}p1 images/bootmnt/

echo "Copy root and boot folders"
mkdir -p $ROOT_FOLDER
mkdir -p $BOOT_FOLDER
#cp -ar images/rootmnt/* $ROOT_FOLDER/
#rm -f $ROOT_FOLDER/boot/overlays
#cp -ar --remove-destination images/bootmnt/* $ROOT_FOLDER/boot
rsync -xaHAXE images/rootmnt/ $ROOT_FOLDER/
rsync -xaHAXE images/bootmnt/ $FIRMWARE_FOLDER

echo "Done, unmounting image."
umount images/rootmnt/
umount images/bootmnt/
kpartx -dv "images/${RASPBIAN_IMG}.img"

echo "Make tftp folder"
sed "/$SERIAL/d" -i /etc/fstab
echo "$FIRMWARE_FOLDER $BOOT_FOLDER none defaults,bind 0 0" | tee -a /etc/fstab
systemctl daemon-reload
mount "$BOOT_FOLDER"

sed "/$SERIAL/d" -i /etc/exports
echo "$ROOT_FOLDER *(rw,sync,no_subtree_check,no_root_squash)" | tee -a /etc/exports
systemctl restart rpcbind
systemctl restart nfs-kernel-server

chmod +x raspi_reset.sh
./raspi_reset.sh $1 $2 $3

echo "Device created."
